package org.academiadecodigo.bootcamp.server;

public class Messages {

    public static final String WELCOME = "Welcome to <Academia de Código_> chat server!";
    public static final String SERVER_FULL = "Server is full. Try again later";

    public static final String COMMAND_IDENTIFIER = "/";
    public static final String ERROR = "Error";
    public static final String INVALID_COMMAND = "";

    public static final String JOIN_ALERT = "has joined";
    public static final String LEAVE = "has left the chat";

    public static final String NAME_USAGE = "/name <new_username>";
    public static final String NAME_IN_USE = "name already in use";
    public static final String RENAME = "is now called";
    public static final String INVALID_USERNAME = "invalid username";

    public static final String WHISPER_USAGE = "/whisper <destination_username> <message>";
    public static final String WHISPER = "(whisper)";

    public static final String NEW_ROUND_IDENTIFIER = "/nr\n";
    public static final String ANSWERS_PHASE_IDENTIFIER = "/ans ";
    public static final String WINNER_PHASE_IDENTIFIER = "/win ";
    public static final String WAITING_FOR_PLAYERS = "Waiting for players...";
    public static final String GAME_IS_ON = "A game is already up, you have to wait till it ends.";
    public static final String NOT_ENOUGH_PLAYERS = "Not enough players to start a game, we need at least 3";
    public static final String SEND_CZAR = " is the card Czar!";
    public static final String IS_WINNER = " has won the round!";

}
