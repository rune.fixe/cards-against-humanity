package org.academiadecodigo.bootcamp.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {

    private static final String DEFAULT_NAME = "player-";
    private static final int MAXIMUM_CLIENTS = 4;


    private Game game;
    private ServerSocket socket;
    private ExecutorService service;
    private final List<ClientConnection> clients;
    private Map<String,String> answermap;

    public Server(int port) throws IOException {
        socket = new ServerSocket(port);
        clients = Collections.synchronizedList(new LinkedList<>());
        service = Executors.newCachedThreadPool();
        answermap = new HashMap<>();

    }

    public void start() throws IOException{
        int connections = 0;

        while (connections < MAXIMUM_CLIENTS) {
            connections++;
            System.out.println("one in");
            waitConnection(connections);
        }
        newGame();

    }


    private void waitConnection(int connections) {
        try {
            Socket clientSocket = socket.accept();

            ClientConnection connection = new ClientConnection(clientSocket, this, DEFAULT_NAME + connections);
            service.submit(connection);


        } catch (IOException e) {
            System.err.println("Error establishing connection: " + e.getMessage());
        }
    }


    public boolean addClient(ClientConnection client) {
        synchronized (clients) {

            if (clients.size() >= MAXIMUM_CLIENTS) {
                return false;
            }

            broadcast(client.getName() + " " + Messages.JOIN_ALERT);
            clients.add(client);
            return true;
        }
    }


    public void broadcast(String message) {
        synchronized (clients) {
            for (ClientConnection client : clients) {
                client.send(message);
            }
        }
    }

    public synchronized void announceCzar(int currentCzar) {

        broadcast(Messages.NEW_ROUND_IDENTIFIER + clients.get(currentCzar).getName() + Messages.SEND_CZAR);
    }

    public synchronized void sendCards() {

            String[] key = (String[]) answermap.keySet().toArray();
            String[] values = new String[key.length];

            for(int i = 0; i < values.length; i++){
                values[i] = answermap.get(key[i]);
            }

            for(String m : values) {

                broadcast(m);
            }

    }

    public boolean getWinner(String message) {

        for(ClientConnection c : clients){

            if(answermap.get(c.getName()).equals(message)) {

                c.addPoint();
                broadcast(c.getName() + Messages.IS_WINNER);
                return true;
            }
        }
        return false;

    }

    public void remove(ClientConnection client) {
        clients.remove(client);
    }

    public String listClients() {
        StringBuilder list = new StringBuilder("Connected Clients:\n");

        synchronized (clients) {
            for (ClientConnection client : clients) {
                list.append(client.getName()).append("\n");
            }
        }

        return list.toString();
    }

    public ClientConnection getClientByName(String name) {
        synchronized (clients) {
            for (ClientConnection client : clients) {
                if (client.getName().equals(name)) {
                    return client;
                }
            }
        }

        return null;
    }

    public void newGame() {

        game = new Game(clients, this);
        service.submit(game);

    }

    public boolean gameIsOn() {

        return game != null;
    }

    //review
    public void saveAnswer(String sender, String message){

        answermap.put(sender, message); //saves sender's answer in answermap;
    }

    //review
    public boolean answersInMap(){
        return answermap.size() < (clients.size() - 1);
    }

    public Map<String,String> getAnswermap(){
        return answermap;
    }
}