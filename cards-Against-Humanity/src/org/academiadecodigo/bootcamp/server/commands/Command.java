package org.academiadecodigo.bootcamp.server.commands;

import org.academiadecodigo.bootcamp.server.Messages;

public enum Command {
    INVALID("", new InvalidHandler()),
    LIST("plist", new ListHandler()),
    QUIT("quit", new QuitHandler()),
    CARD_PHASE("ans", new answerHandler()),
    WIN_PHASE("win", new WinnerHandler());



    private String commandMessage;
    private CommandHandler handler;

    Command(String commandMessage, CommandHandler handler) {
        this.commandMessage = Messages.COMMAND_IDENTIFIER + commandMessage;
        this.handler = handler;
    }

    public static Command getFromString(String message) {

        if (message == null) {
            return QUIT;
        }

        String userCommand = message.split(" ")[0];

        for (Command command : values()) {
            if (userCommand.equals(command.commandMessage)) {
                return command;
            }
        }

        return INVALID;
    }

    public CommandHandler getHandler() {
        return handler;
    }
}