package org.academiadecodigo.bootcamp.server.commands;

import org.academiadecodigo.bootcamp.server.ClientConnection;
import org.academiadecodigo.bootcamp.server.Server;

public class WinnerHandler implements CommandHandler {

    @Override
    public void handle(Server server, ClientConnection sender, String message) {

        server.getWinner(message);
        sender.setHasAnswered();
    }
}
