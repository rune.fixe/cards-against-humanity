package org.academiadecodigo.bootcamp.server.commands;

import java.util.Arrays;

public enum BlackCards {

    CARD_1("____ as a service."),
    CARD_2("________ dooms any project to epic failure."),
    CARD_3("____ is a feature, not a bug. "),
    CARD_4("Smashing the _____ for fun and profit."),
    CARD_5("_____ is weaponized privilege."),
    CARD_6("____ is webscale."),
    CARD_7("_____ logs or it didn't happen."),
    CARD_8("_____ with one weird trick."),
    CARD_9("____ as one does."),
    CARD_10("_____, for some values of ____ ."),
    CARD_11("Ask me anything about ______."),
    CARD_12("Called on account of _______.");

    private String cardText;

    BlackCards(String s) {
        cardText = s;
    }


    public static String getRandomCard() {

        int index = (int) Math.ceil(BlackCards.values().length * Math.random());

                return BlackCards.values()[index].getString();


    }

    public String getString() {

        return cardText;
    }


}
