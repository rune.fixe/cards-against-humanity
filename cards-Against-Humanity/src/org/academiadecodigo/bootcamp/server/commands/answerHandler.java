package org.academiadecodigo.bootcamp.server.commands;

import org.academiadecodigo.bootcamp.server.ClientConnection;
import org.academiadecodigo.bootcamp.server.Server;

public class answerHandler implements CommandHandler {

    @Override
    public void handle(Server server, ClientConnection sender, String message) {


        server.saveAnswer(sender.getName(), message);
    }
}
