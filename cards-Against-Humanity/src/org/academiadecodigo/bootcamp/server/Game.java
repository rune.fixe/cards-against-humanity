package org.academiadecodigo.bootcamp.server;

import org.academiadecodigo.bootcamp.server.commands.BlackCards;

import java.util.List;

public class Game implements Runnable {

    private final static int MAX_POINTS = 5;

    private List<ClientConnection> clients;
    private int currentCzar;
    private Server server;


    public Game(List<ClientConnection> clients, Server server) {

        this.clients = clients;
        this.server = server;

    }

    @Override
    public void run() {

        chooseFirstCzar();

        while (!gameOver()) {

            playRound();
            nextCzar();
        }

    }

    public void chooseFirstCzar() {

        currentCzar = (int) (Math.random() * (clients.size() + 1));
        clients.get(currentCzar).setIsCzar(true);

    }

    public void nextCzar() {

        clients.get(currentCzar).setIsCzar(false);
        currentCzar++;

        if (currentCzar >= clients.size()) {

            currentCzar = 0;

        }
        clients.get(currentCzar).setIsCzar(true);

    }

    public boolean gameOver() {

        boolean isOver = false;

        for (ClientConnection i : clients) {

            if (i.getPoints() == MAX_POINTS) {

                isOver = true;
            }
        }

        return isOver;
    }

    public void playRound() {

        server.announceCzar(currentCzar);

        server.broadcast(BlackCards.getRandomCard());

        while (!server.answersInMap()) {

        }
        server.sendCards();

        while (!clients.get(currentCzar).getHasAnswered()) {

        }


    }



}




