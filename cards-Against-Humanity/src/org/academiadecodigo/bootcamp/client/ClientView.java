package org.academiadecodigo.bootcamp.client;
import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.menu.MenuInputScanner;
import org.academiadecodigo.bootcamp.scanners.string.StringInputScanner;
import java.io.*;
import java.util.LinkedList;
import static org.academiadecodigo.bootcamp.client.ClientMessages.*;


public class ClientView {


    private Prompt prompt;
    private Client client;
    private ClientPrinter printer;

    public ClientView(Client client) {
        this.client = client;
        this.prompt = new Prompt(System.in, System.out);
    }

    public void show (BufferedReader reader) throws IOException{

        printer = new ClientPrinter(reader);
        //Thread printThread = new Thread(printer);

        String line = reader.readLine();
        System.out.println(line);

        // czar condition
        if (line.startsWith(client.getName())){
            System.out.println(reader.readLine());
            //print(reader);
            //printer.run();

            System.out.println(WAITING_REPLIES);

            //if(!(line = reader.readLine()).startsWith("/win"));

            //client.writer(WINNER + scanCzar(reader));
        }

        //player condition
        // print(reader);
        //printer.run();
        System.out.println(reader.readLine());

        client.writer(REPLY + scanInput());

        System.out.println(WAITING_PLAYERS);

        /*while (!reader.readLine().startsWith("/win"));

        //print(reader);
        printer.run();

        System.out.println(WAITING_CZAR);*/

    }


    protected String scanInput() {
        StringInputScanner scanner = new StringInputScanner();
        scanner.setMessage(ClientMessages.FREE_INPUT);

        return prompt.getUserInput(scanner);
    }


    protected String scanCzar (BufferedReader reader) throws IOException{

        LinkedList<String> replies = new LinkedList<>();

        String line = reader.readLine();

        while (!line.isEmpty()) {
            replies.add(line);
            line = reader.readLine();
        }

        String[] choices = (String[]) replies.toArray();

        MenuInputScanner mainMenu = new MenuInputScanner(choices);
        mainMenu.setError(ClientMessages.ERROR_INVALID_OPTION);
        mainMenu.setMessage(ClientMessages.CZAR_HEADER);

        return choices[prompt.getUserInput(mainMenu)];
    }

    protected void print(BufferedReader reader) {


        try {

            String line = reader.readLine();
            line = reader.readLine();
            System.out.println(line);

            System.out.println("LET ME IN");
            while (true) {
                System.out.println("GET ME OUT OF HERE");
                System.out.println(line);

            System.out.println("IM SAVED");
            }
        } catch (IOException e){
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

}
