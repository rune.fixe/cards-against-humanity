package org.academiadecodigo.bootcamp.client;
import java.io.*;
import java.net.Socket;
import static org.academiadecodigo.bootcamp.client.ClientMessages.NEW_ROUND;


public class Client {


    private Socket socket;
    private ClientView view;
    private String name;
    private BufferedReader reader;
    private BufferedWriter writer;

    public Client(String host, int port) throws IOException {

        view = new ClientView(this);

        socket = new Socket(host, port);
        writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        System.out.println(reader.readLine());

    }

    public void start() {

        try {

            name = reader.readLine().split(" ")[0];
            System.out.println(name);

            while (!socket.isClosed()) {

                String line = reader.readLine();

                if (line.startsWith("/nr")) {
                    System.out.println("new round");
                    view.show(reader);
                    continue;
                }

                System.out.println(line);
            }

        } catch (IOException e) {
            System.err.println("Error handling socket connection: " + e.getMessage());
        }
    }


    protected void writer(String toWrite) {
        try {
            writer.write(toWrite);

        } catch (IOException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    /*private void waitMessage(String message){

        if (message == null) {
            System.out.println("Connection closed from server side");
            System.exit(0);
        }
    } */

    protected String getName() {
        return name;
    }
}
