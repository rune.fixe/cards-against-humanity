package org.academiadecodigo.bootcamp.client;

/**
 * Created by codecadet on 16/11/2019.
 */
public class ClientMessages {

    public static final String WELCOME = "Welcome to Cards Against Humanity: Programmer Edition!";

    public static final String FREE_INPUT = "What's your answer?";
    public static final String ERROR_INVALID_OPTION = "That's not a valid choice!";
    public static final String WAITING_REPLIES = "Waiting for all players...";
    public static final String WAITING_CZAR = "Waiting for card czar to choose a winner...";
    public static final String WAITING_PLAYERS = "Waiting for other players...";

    public static final String CZAR_HEADER = "Choose the winner!";

    //signals
    public static final String NEW_ROUND = "/nr ";
    public static final String REPLY = "/ans ";
    public static final String WINNER = "/win ";

    private static final String EXIT = "/quit";
}
