package org.academiadecodigo.bootcamp.client;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * Created by codecadet on 17/11/2019.
 */
public class ClientPrinter extends Thread {

    private BufferedReader reader;

    public ClientPrinter(BufferedReader reader) {
        this.reader = reader;
    }


    public void run() {

        try {

            String line;

            System.out.println("LET ME IN");

            while ((line = reader.readLine()).isEmpty()) {
                System.out.println("GET ME OUT OF HERE");
                System.out.println(line);
            }

        } catch (IOException e){
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
}
